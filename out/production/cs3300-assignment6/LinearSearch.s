.text
.globl  main
main:
move $fp $sp
subu $sp $sp 76
sw $ra -4($fp)
li $t9 16
move $a0 $t9
jal _halloc
move $t8 $v0
move $t7 $t8
li $t6 12
move $a0 $t6
jal _halloc
move $t5 $v0
move $t4 $t5
la $t3 LS_Init
sw $t3 12($t7)
la $t2 LS_Search
sw $t2 8($t7)
la $t1 LS_Print
sw $t1 4($t7)
la $t0 LS_Start
sw $t0 0($t7)
li $s7 4
move $s6 $s7
L0: nop
li $s5 11
sle $s4 $s6 $s5
beqz $s4 L1
add $s3 $t4 $s6
li $s2 0
sw $s2 0($s3)
li $s1 4
add $s0 $s6 $s1
move $s6 $s0
b L0
L1: nop
sw $t7 0($t4)
move $t9 $t4
lw $t8 0($t9)
lw $t6 0($t8)
li $t5 10
move $a0 $t9
move $a1 $t5
jalr $t6
move $t3 $v0
move $a0 $t3
jal _print
lw $ra -4($fp)
addu $sp $sp 76
j $ra
.text
.globl  LS_Start
LS_Start:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 80
sw $ra -4($fp)
sw $t4 0($sp)
sw $t5 4($sp)
sw $t6 8($sp)
sw $t7 12($sp)
sw $t8 16($sp)
sw $t9 20($sp)
sw $s0 24($sp)
sw $s1 28($sp)
sw $s2 32($sp)
sw $s3 36($sp)
sw $s4 40($sp)
sw $s5 44($sp)
sw $s6 48($sp)
sw $s7 52($sp)
sw $t0 56($sp)
sw $t1 60($sp)
sw $t2 64($sp)
sw $t3 68($sp)
move $t8 $a0
move $t9 $a1
move $t7 $t8
lw $t6 0($t7)
lw $t5 12($t6)
move $a0 $t7
move $a1 $t9
jalr $t5
move $t4 $v0
move $t3 $t8
lw $t2 0($t3)
lw $t1 4($t2)
move $a0 $t3
jalr $t1
move $t0 $v0
li $s7 9999
move $a0 $s7
jal _print
move $s6 $t8
lw $s5 0($s6)
lw $s4 8($s5)
li $s3 8
move $a0 $s6
move $a1 $s3
jalr $s4
move $s2 $v0
move $a0 $s2
jal _print
move $s1 $t8
lw $s0 0($s1)
lw $t6 8($s0)
li $t9 12
move $a0 $s1
move $a1 $t9
jalr $t6
move $t5 $v0
move $a0 $t5
jal _print
move $t7 $t8
lw $t4 0($t7)
lw $t2 8($t4)
li $t3 17
move $a0 $t7
move $a1 $t3
jalr $t2
move $t1 $v0
move $a0 $t1
jal _print
move $t0 $t8
lw $s7 0($t0)
lw $s5 8($s7)
li $s6 50
move $a0 $t0
move $a1 $s6
jalr $s5
move $s3 $v0
move $a0 $s3
jal _print
li $s4 55
move $v0 $s4
lw $t4 0($sp)
lw $t5 4($sp)
lw $t6 8($sp)
lw $t7 12($sp)
lw $t8 16($sp)
lw $t9 20($sp)
lw $s0 24($sp)
lw $s1 28($sp)
lw $s2 32($sp)
lw $s3 36($sp)
lw $s4 40($sp)
lw $s5 44($sp)
lw $s6 48($sp)
lw $s7 52($sp)
lw $t0 56($sp)
lw $t1 60($sp)
lw $t2 64($sp)
lw $t3 68($sp)
lw $ra -4($fp)
lw $fp 72($sp)
addu $sp $sp 80
j $ra
.text
.globl  LS_Print
LS_Print:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 80
sw $ra -4($fp)
sw $t4 0($sp)
sw $t5 4($sp)
sw $t6 8($sp)
sw $t7 12($sp)
sw $t8 16($sp)
sw $t9 20($sp)
sw $s0 24($sp)
sw $s1 28($sp)
sw $s2 32($sp)
sw $s3 36($sp)
sw $s4 40($sp)
sw $s5 44($sp)
sw $s6 48($sp)
sw $s7 52($sp)
sw $t0 56($sp)
sw $t1 60($sp)
sw $t2 64($sp)
sw $t3 68($sp)
move $t9 $a0
li $t8 1
move $t7 $t8
L2: nop
lw $t6 8($t9)
li $t5 1
sub $t4 $t6 $t5
sle $t3 $t7 $t4
beqz $t3 L3
lw $s7 4($t9)
li $t2 4
mul $t1 $t7 $t2
move $t0 $t1
lw $s7 4($t9)
lw $s6 0($s7)
li $s5 1
li $s4 1
sub $s3 $s6 $s4
sle $s2 $t0 $s3
sub $s1 $s5 $s2
beqz $s1 L4
L4: nop
li $s0 4
add $t8 $t0 $s0
add $t6 $s7 $t8
lw $t5 0($t6)
move $a0 $t5
jal _print
li $t4 1
add $t3 $t7 $t4
move $t7 $t3
b L2
L3: nop
li $t2 0
move $v0 $t2
lw $t4 0($sp)
lw $t5 4($sp)
lw $t6 8($sp)
lw $t7 12($sp)
lw $t8 16($sp)
lw $t9 20($sp)
lw $s0 24($sp)
lw $s1 28($sp)
lw $s2 32($sp)
lw $s3 36($sp)
lw $s4 40($sp)
lw $s5 44($sp)
lw $s6 48($sp)
lw $s7 52($sp)
lw $t0 56($sp)
lw $t1 60($sp)
lw $t2 64($sp)
lw $t3 68($sp)
lw $ra -4($fp)
lw $fp 72($sp)
addu $sp $sp 80
j $ra
.text
.globl  LS_Search
LS_Search:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 80
sw $ra -4($fp)
sw $t4 0($sp)
sw $t5 4($sp)
sw $t6 8($sp)
sw $t7 12($sp)
sw $t8 16($sp)
sw $t9 20($sp)
sw $s0 24($sp)
sw $s1 28($sp)
sw $s2 32($sp)
sw $s3 36($sp)
sw $s4 40($sp)
sw $s5 44($sp)
sw $s6 48($sp)
sw $s7 52($sp)
sw $t0 56($sp)
sw $t1 60($sp)
sw $t2 64($sp)
sw $t3 68($sp)
move $t8 $a0
move $t9 $a1
li $t7 1
move $t6 $t7
li $t5 0
li $t4 0
move $t3 $t4
L5: nop
lw $t2 8($t8)
li $t1 1
sub $t0 $t2 $t1
sle $s7 $t6 $t0
beqz $s7 L6
lw $s3 4($t8)
li $s6 4
mul $s5 $t6 $s6
move $s4 $s5
lw $s3 4($t8)
lw $s2 0($s3)
li $s1 1
li $s0 1
sub $t7 $s2 $s0
sle $t5 $s4 $t7
sub $t4 $s1 $t5
beqz $t4 L7
L7: nop
li $t2 4
add $t1 $s4 $t2
add $t0 $s3 $t1
lw $s7 0($t0)
move $s6 $s7
li $s5 1
add $s2 $t9 $s5
move $s0 $s2
li $t7 1
sub $t5 $t9 $t7
sle $s1 $s6 $t5
beqz $s1 L8
li $t4 0
b L9
L8: nop
li $t2 1
li $s4 1
sub $t1 $s0 $s4
sle $s3 $s6 $t1
sub $t0 $t2 $s3
beqz $t0 L10
li $s7 0
b L11
L10: nop
li $s5 1
li $s2 1
move $t3 $s2
lw $t7 8($t8)
move $t6 $t7
L11: nop
L9: nop
li $t5 1
add $s1 $t6 $t5
move $t6 $s1
b L5
L6: nop
move $v0 $t3
lw $t4 0($sp)
lw $t5 4($sp)
lw $t6 8($sp)
lw $t7 12($sp)
lw $t8 16($sp)
lw $t9 20($sp)
lw $s0 24($sp)
lw $s1 28($sp)
lw $s2 32($sp)
lw $s3 36($sp)
lw $s4 40($sp)
lw $s5 44($sp)
lw $s6 48($sp)
lw $s7 52($sp)
lw $t0 56($sp)
lw $t1 60($sp)
lw $t2 64($sp)
lw $t3 68($sp)
lw $ra -4($fp)
lw $fp 72($sp)
addu $sp $sp 80
j $ra
.text
.globl  LS_Init
LS_Init:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 80
sw $ra -4($fp)
sw $s0 0($sp)
sw $s1 4($sp)
sw $s2 8($sp)
sw $s3 12($sp)
sw $s4 16($sp)
sw $s5 20($sp)
sw $s6 24($sp)
sw $s7 28($sp)
sw $t0 32($sp)
sw $t1 36($sp)
sw $t2 40($sp)
sw $t3 44($sp)
sw $t4 48($sp)
sw $t5 52($sp)
sw $t6 56($sp)
sw $t7 60($sp)
sw $t8 64($sp)
sw $t9 68($sp)
move $t8 $a0
move $t9 $a1
sw $t9 8($t8)
li $t7 1
add $t6 $t9 $t7
li $t5 4
mul $t4 $t6 $t5
move $a0 $t4
jal _halloc
move $t3 $v0
move $t2 $t3
li $t1 4
move $t0 $t1
L12: nop
li $s7 1
add $s6 $t9 $s7
li $s5 4
mul $s4 $s6 $s5
li $s3 1
sub $s2 $s4 $s3
sle $s1 $t0 $s2
beqz $s1 L13
add $s0 $t2 $t0
li $t7 0
sw $t7 0($s0)
li $t6 4
add $t5 $t0 $t6
move $t0 $t5
b L12
L13: nop
li $t4 4
mul $t3 $t9 $t4
sw $t3 0($t2)
sw $t2 4($t8)
li $t1 1
move $s7 $t1
lw $s6 8($t8)
li $s5 1
add $s4 $s6 $s5
move $s3 $s4
L14: nop
lw $s2 8($t8)
li $s1 1
sub $s0 $s2 $s1
sle $t7 $s7 $s0
beqz $t7 L15
li $t6 2
mul $t0 $t6 $s7
move $t5 $t0
li $t4 3
sub $t9 $s3 $t4
move $t2 $t9
li $t3 1
li $t1 4
mul $s6 $t3 $t1
move $s5 $s6
add $s4 $t8 $s5
lw $t9 0($s4)
li $s2 4
mul $s1 $s7 $s2
move $s0 $s1
li $t7 1
li $t6 4
mul $t0 $t7 $t6
move $s5 $t0
add $t4 $t8 $s5
lw $t9 0($t4)
lw $t3 0($t9)
li $t1 1
li $s6 1
sub $s4 $t3 $s6
sle $s2 $s0 $s4
sub $s1 $t1 $s2
beqz $s1 L16
L16: nop
li $t7 4
add $t6 $s0 $t7
add $s5 $t9 $t6
add $t0 $t5 $t2
sw $t0 0($s5)
li $t4 1
add $t3 $s7 $t4
move $s7 $t3
li $s6 1
sub $s4 $s3 $s6
move $s3 $s4
b L14
L15: nop
li $s2 0
move $v0 $s2
lw $s0 0($sp)
lw $s1 4($sp)
lw $s2 8($sp)
lw $s3 12($sp)
lw $s4 16($sp)
lw $s5 20($sp)
lw $s6 24($sp)
lw $s7 28($sp)
lw $t0 32($sp)
lw $t1 36($sp)
lw $t2 40($sp)
lw $t3 44($sp)
lw $t4 48($sp)
lw $t5 52($sp)
lw $t6 56($sp)
lw $t7 60($sp)
lw $t8 64($sp)
lw $t9 68($sp)
lw $ra -4($fp)
lw $fp 72($sp)
addu $sp $sp 80
j $ra
.text
         .globl _halloc
_halloc:
         li $v0, 9
         syscall
         j $ra

         .text
         .globl _print
_print:
         li $v0, 1
         syscall
         la $a0, newl
         li $v0, 4
         syscall
         j $ra

         .data
         .align   0
newl:    .asciiz "\n" 
         .data
         .align   0
str_er:  .asciiz " ERROR: abnormal termination\n" 
