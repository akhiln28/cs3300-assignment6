.text
.globl  main
main:
move $fp $sp
subu $sp $sp 4
sw $ra -4($fp)
jal Test35
move $t8 $v0
move $a0 $t8
jal _print
lw $ra -4($fp)
addu $sp $sp 4
j $ra
.text
.globl  Test35
Test35:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 144
sw $ra -4($fp)
sw $s0 64($sp)
sw $s1 68($sp)
sw $s2 72($sp)
sw $s3 76($sp)
sw $s4 80($sp)
sw $s5 84($sp)
sw $s6 88($sp)
sw $s7 92($sp)
sw $t0 96($sp)
sw $t1 100($sp)
sw $t2 104($sp)
sw $t3 108($sp)
sw $t4 112($sp)
sw $t5 116($sp)
sw $t6 120($sp)
sw $t7 124($sp)
sw $t8 128($sp)
sw $t9 132($sp)
li $v0 0
sw $v0 0($sp)
li $v0 1
sw $v0 4($sp)
li $v0 2
sw $v0 8($sp)
li $v0 3
sw $v0 12($sp)
li $v0 4
sw $v0 16($sp)
li $v0 5
sw $v0 20($sp)
li $v0 6
sw $v0 24($sp)
li $v0 7
sw $v0 28($sp)
li $v0 8
sw $v0 32($sp)
li $v0 9
sw $v0 36($sp)
li $v0 10
sw $v0 40($sp)
li $v0 11
sw $v0 44($sp)
li $v0 12
sw $v0 48($sp)
li $v0 13
sw $v0 52($sp)
li $v0 14
sw $v0 56($sp)
li $v0 15
sw $v0 60($sp)
li $s1 16
li $s0 17
li $t9 18
li $t8 19
li $t7 20
li $t6 21
li $t5 22
li $t4 23
li $t3 24
li $t2 25
li $t1 26
li $t0 27
li $s7 28
li $s6 29
li $s5 30
li $s4 31
li $s3 32
li $s2 33
move $a0 $s2
jal _print
move $a0 $s3
jal _print
move $a0 $s4
jal _print
move $a0 $s5
jal _print
move $a0 $s6
jal _print
move $a0 $s7
jal _print
move $a0 $t0
jal _print
move $a0 $t1
jal _print
move $a0 $t2
jal _print
move $a0 $t3
jal _print
move $a0 $t4
jal _print
move $a0 $t5
jal _print
move $a0 $t6
jal _print
move $a0 $t7
jal _print
move $a0 $t8
jal _print
move $a0 $t9
jal _print
move $a0 $s0
jal _print
move $a0 $s1
jal _print
lw $v0 60($sp)
move $a0 $v0
jal _print
lw $v0 56($sp)
move $a0 $v0
jal _print
lw $v0 52($sp)
move $a0 $v0
jal _print
lw $v0 48($sp)
move $a0 $v0
jal _print
lw $v0 44($sp)
move $a0 $v0
jal _print
lw $v0 40($sp)
move $a0 $v0
jal _print
lw $v0 36($sp)
move $a0 $v0
jal _print
lw $v0 32($sp)
move $a0 $v0
jal _print
lw $v0 28($sp)
move $a0 $v0
jal _print
lw $v0 24($sp)
move $a0 $v0
jal _print
lw $v0 20($sp)
move $a0 $v0
jal _print
lw $v0 16($sp)
move $a0 $v0
jal _print
lw $v0 12($sp)
move $a0 $v0
jal _print
lw $v0 8($sp)
move $a0 $v0
jal _print
lw $v0 4($sp)
move $a0 $v0
jal _print
lw $v0 0($sp)
move $a0 $v0
jal _print
lw $v0 0($sp)
move $v0 $v0
lw $s0 64($sp)
lw $s1 68($sp)
lw $s2 72($sp)
lw $s3 76($sp)
lw $s4 80($sp)
lw $s5 84($sp)
lw $s6 88($sp)
lw $s7 92($sp)
lw $t0 96($sp)
lw $t1 100($sp)
lw $t2 104($sp)
lw $t3 108($sp)
lw $t4 112($sp)
lw $t5 116($sp)
lw $t6 120($sp)
lw $t7 124($sp)
lw $t8 128($sp)
lw $t9 132($sp)
lw $ra -4($fp)
lw $fp 136($sp)
addu $sp $sp 144
j $ra
.text
         .globl _halloc
_halloc:
         li $v0, 9
         syscall
         j $ra

         .text
         .globl _print
_print:
         li $v0, 1
         syscall
         la $a0, newl
         li $v0, 4
         syscall
         j $ra

         .data
         .align   0
newl:    .asciiz "\n" 
         .data
         .align   0
str_er:  .asciiz " ERROR: abnormal termination\n" 
