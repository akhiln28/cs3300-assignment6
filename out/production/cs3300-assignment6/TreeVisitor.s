.text
.globl  main
main:
move $fp $sp
subu $sp $sp 48
sw $ra -4($fp)
li $t9 4
move $a0 $t9
jal _halloc
move $t8 $v0
move $t7 $t8
li $t6 4
move $a0 $t6
jal _halloc
move $t5 $v0
move $t4 $t5
la $t3 TV_Start
sw $t3 0($t7)
sw $t7 0($t4)
move $t2 $t4
lw $t1 0($t2)
lw $t0 0($t1)
move $a0 $t2
jalr $t0
move $s7 $v0
move $a0 $s7
jal _print
lw $ra -4($fp)
addu $sp $sp 48
j $ra
.text
.globl  TV_Start
TV_Start:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 80
sw $ra -4($fp)
sw $s0 0($sp)
sw $s1 4($sp)
sw $s2 8($sp)
sw $s3 12($sp)
sw $s4 16($sp)
sw $s5 20($sp)
sw $s6 24($sp)
sw $s7 28($sp)
sw $t0 32($sp)
sw $t1 36($sp)
sw $t2 40($sp)
sw $t3 44($sp)
sw $t4 48($sp)
sw $t5 52($sp)
sw $t6 56($sp)
sw $t7 60($sp)
sw $t8 64($sp)
sw $t9 68($sp)
li $t9 84
move $a0 $t9
jal _halloc
move $t8 $v0
move $t7 $t8
li $t6 28
move $a0 $t6
jal _halloc
move $t5 $v0
move $t4 $t5
la $t3 Tree_accept
sw $t3 80($t7)
la $t2 Tree_RecPrint
sw $t2 76($t7)
la $t1 Tree_Print
sw $t1 72($t7)
la $t0 Tree_Search
sw $t0 68($t7)
la $s7 Tree_RemoveLeft
sw $s7 64($t7)
la $s6 Tree_RemoveRight
sw $s6 60($t7)
la $s5 Tree_Remove
sw $s5 56($t7)
la $s4 Tree_Delete
sw $s4 52($t7)
la $s3 Tree_Insert
sw $s3 48($t7)
la $s2 Tree_Compare
sw $s2 44($t7)
la $s1 Tree_SetHas_Right
sw $s1 40($t7)
la $s0 Tree_SetHas_Left
sw $s0 36($t7)
la $t9 Tree_GetHas_Left
sw $t9 32($t7)
la $t8 Tree_GetHas_Right
sw $t8 28($t7)
la $t6 Tree_SetKey
sw $t6 24($t7)
la $t5 Tree_GetKey
sw $t5 20($t7)
la $t3 Tree_GetLeft
sw $t3 16($t7)
la $t2 Tree_GetRight
sw $t2 12($t7)
la $t1 Tree_SetLeft
sw $t1 8($t7)
la $t0 Tree_SetRight
sw $t0 4($t7)
la $s7 Tree_Init
sw $s7 0($t7)
li $s6 4
move $s5 $s6
L2: nop
li $s4 27
sle $s3 $s5 $s4
beqz $s3 L3
add $s2 $t4 $s5
li $s1 0
sw $s1 0($s2)
li $s0 4
add $t9 $s5 $s0
move $s5 $t9
b L2
L3: nop
sw $t7 0($t4)
move $t8 $t4
move $t6 $t8
lw $t5 0($t6)
lw $t3 0($t5)
li $t2 16
move $a0 $t6
move $a1 $t2
jalr $t3
move $t1 $v0
move $t0 $t8
lw $s7 0($t0)
lw $s6 72($s7)
move $a0 $t0
jalr $s6
move $s4 $v0
li $s3 100000000
move $a0 $s3
jal _print
move $s2 $t8
lw $s1 0($s2)
lw $s0 48($s1)
li $s5 8
move $a0 $s2
move $a1 $s5
jalr $s0
move $t4 $v0
move $t9 $t8
lw $t7 0($t9)
lw $t5 48($t7)
li $t6 24
move $a0 $t9
move $a1 $t6
jalr $t5
move $t2 $v0
move $t3 $t8
lw $t1 0($t3)
lw $s7 48($t1)
li $t0 4
move $a0 $t3
move $a1 $t0
jalr $s7
move $s6 $v0
move $s4 $t8
lw $s3 0($s4)
lw $s1 48($s3)
li $s2 12
move $a0 $s4
move $a1 $s2
jalr $s1
move $s5 $v0
move $s0 $t8
lw $t4 0($s0)
lw $t7 48($t4)
li $t9 20
move $a0 $s0
move $a1 $t9
jalr $t7
move $t6 $v0
move $t5 $t8
lw $t2 0($t5)
lw $t1 48($t2)
li $t3 28
move $a0 $t5
move $a1 $t3
jalr $t1
move $t0 $v0
move $s7 $t8
lw $s6 0($s7)
lw $s3 48($s6)
li $s4 14
move $a0 $s7
move $a1 $s4
jalr $s3
move $s2 $v0
move $s1 $t8
lw $s5 0($s1)
lw $t4 72($s5)
move $a0 $s1
jalr $t4
move $s0 $v0
li $t9 100000000
move $a0 $t9
jal _print
li $t7 4
move $a0 $t7
jal _halloc
move $t6 $v0
move $t2 $t6
li $t5 12
move $a0 $t5
jal _halloc
move $t3 $v0
move $t1 $t3
la $t0 MyVisitor_visit
sw $t0 0($t2)
li $s6 4
move $s7 $s6
L4: nop
li $s4 11
sle $s3 $s7 $s4
beqz $s3 L5
add $s2 $t1 $s7
li $s5 0
sw $s5 0($s2)
li $s1 4
add $t4 $s7 $s1
move $s7 $t4
b L4
L5: nop
sw $t2 0($t1)
move $s0 $t1
li $t9 50000000
move $a0 $t9
jal _print
move $t7 $t8
lw $t6 0($t7)
lw $t5 80($t6)
move $a0 $t7
move $a1 $s0
jalr $t5
move $t3 $v0
li $t0 100000000
move $a0 $t0
jal _print
move $s6 $t8
lw $s4 0($s6)
lw $s3 68($s4)
li $s2 24
move $a0 $s6
move $a1 $s2
jalr $s3
move $s5 $v0
move $a0 $s5
jal _print
move $s1 $t8
lw $s7 0($s1)
lw $t1 68($s7)
li $t4 12
move $a0 $s1
move $a1 $t4
jalr $t1
move $t2 $v0
move $a0 $t2
jal _print
move $t9 $t8
lw $t6 0($t9)
lw $s0 68($t6)
li $t5 16
move $a0 $t9
move $a1 $t5
jalr $s0
move $t7 $v0
move $a0 $t7
jal _print
move $t3 $t8
lw $t0 0($t3)
lw $s4 68($t0)
li $s6 50
move $a0 $t3
move $a1 $s6
jalr $s4
move $s2 $v0
move $a0 $s2
jal _print
move $s3 $t8
lw $s5 0($s3)
lw $s7 68($s5)
li $s1 12
move $a0 $s3
move $a1 $s1
jalr $s7
move $t4 $v0
move $a0 $t4
jal _print
move $t1 $t8
lw $t2 0($t1)
lw $t6 52($t2)
li $t9 12
move $a0 $t1
move $a1 $t9
jalr $t6
move $t5 $v0
move $s0 $t8
lw $t7 0($s0)
lw $t0 72($t7)
move $a0 $s0
jalr $t0
move $t3 $v0
move $s6 $t8
lw $s4 0($s6)
lw $s2 68($s4)
li $s5 12
move $a0 $s6
move $a1 $s5
jalr $s2
move $s3 $v0
move $a0 $s3
jal _print
li $s1 0
move $v0 $s1
lw $s0 0($sp)
lw $s1 4($sp)
lw $s2 8($sp)
lw $s3 12($sp)
lw $s4 16($sp)
lw $s5 20($sp)
lw $s6 24($sp)
lw $s7 28($sp)
lw $t0 32($sp)
lw $t1 36($sp)
lw $t2 40($sp)
lw $t3 44($sp)
lw $t4 48($sp)
lw $t5 52($sp)
lw $t6 56($sp)
lw $t7 60($sp)
lw $t8 64($sp)
lw $t9 68($sp)
lw $ra -4($fp)
lw $fp 72($sp)
addu $sp $sp 80
j $ra
.text
.globl  Tree_Init
Tree_Init:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 28
sw $ra -4($fp)
sw $t5 0($sp)
sw $t6 4($sp)
sw $t7 8($sp)
sw $t8 12($sp)
sw $t9 16($sp)
move $t8 $a0
move $t9 $a1
sw $t9 12($t8)
li $t7 0
sw $t7 16($t8)
li $t6 0
sw $t6 20($t8)
li $t5 1
move $v0 $t5
lw $t5 0($sp)
lw $t6 4($sp)
lw $t7 8($sp)
lw $t8 12($sp)
lw $t9 16($sp)
lw $ra -4($fp)
lw $fp 20($sp)
addu $sp $sp 28
j $ra
.text
.globl  Tree_SetRight
Tree_SetRight:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 20
sw $ra -4($fp)
sw $t7 0($sp)
sw $t8 4($sp)
sw $t9 8($sp)
move $t8 $a0
move $t9 $a1
sw $t9 8($t8)
li $t7 1
move $v0 $t7
lw $t7 0($sp)
lw $t8 4($sp)
lw $t9 8($sp)
lw $ra -4($fp)
lw $fp 12($sp)
addu $sp $sp 20
j $ra
.text
.globl  Tree_SetLeft
Tree_SetLeft:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 20
sw $ra -4($fp)
sw $t7 0($sp)
sw $t8 4($sp)
sw $t9 8($sp)
move $t8 $a0
move $t9 $a1
sw $t9 4($t8)
li $t7 1
move $v0 $t7
lw $t7 0($sp)
lw $t8 4($sp)
lw $t9 8($sp)
lw $ra -4($fp)
lw $fp 12($sp)
addu $sp $sp 20
j $ra
.text
.globl  Tree_GetRight
Tree_GetRight:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 16
sw $ra -4($fp)
sw $t8 0($sp)
sw $t9 4($sp)
move $t9 $a0
lw $t8 8($t9)
move $v0 $t8
lw $t8 0($sp)
lw $t9 4($sp)
lw $ra -4($fp)
lw $fp 8($sp)
addu $sp $sp 16
j $ra
.text
.globl  Tree_GetLeft
Tree_GetLeft:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 16
sw $ra -4($fp)
sw $t8 0($sp)
sw $t9 4($sp)
move $t9 $a0
lw $t8 4($t9)
move $v0 $t8
lw $t8 0($sp)
lw $t9 4($sp)
lw $ra -4($fp)
lw $fp 8($sp)
addu $sp $sp 16
j $ra
.text
.globl  Tree_GetKey
Tree_GetKey:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 16
sw $ra -4($fp)
sw $t8 0($sp)
sw $t9 4($sp)
move $t9 $a0
lw $t8 12($t9)
move $v0 $t8
lw $t8 0($sp)
lw $t9 4($sp)
lw $ra -4($fp)
lw $fp 8($sp)
addu $sp $sp 16
j $ra
.text
.globl  Tree_SetKey
Tree_SetKey:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 20
sw $ra -4($fp)
sw $t7 0($sp)
sw $t8 4($sp)
sw $t9 8($sp)
move $t8 $a0
move $t9 $a1
sw $t9 12($t8)
li $t7 1
move $v0 $t7
lw $t7 0($sp)
lw $t8 4($sp)
lw $t9 8($sp)
lw $ra -4($fp)
lw $fp 12($sp)
addu $sp $sp 20
j $ra
.text
.globl  Tree_GetHas_Right
Tree_GetHas_Right:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 16
sw $ra -4($fp)
sw $t8 0($sp)
sw $t9 4($sp)
move $t9 $a0
lw $t8 20($t9)
move $v0 $t8
lw $t8 0($sp)
lw $t9 4($sp)
lw $ra -4($fp)
lw $fp 8($sp)
addu $sp $sp 16
j $ra
.text
.globl  Tree_GetHas_Left
Tree_GetHas_Left:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 16
sw $ra -4($fp)
sw $t8 0($sp)
sw $t9 4($sp)
move $t9 $a0
lw $t8 16($t9)
move $v0 $t8
lw $t8 0($sp)
lw $t9 4($sp)
lw $ra -4($fp)
lw $fp 8($sp)
addu $sp $sp 16
j $ra
.text
.globl  Tree_SetHas_Left
Tree_SetHas_Left:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 20
sw $ra -4($fp)
sw $t7 0($sp)
sw $t8 4($sp)
sw $t9 8($sp)
move $t8 $a0
move $t9 $a1
sw $t9 16($t8)
li $t7 1
move $v0 $t7
lw $t7 0($sp)
lw $t8 4($sp)
lw $t9 8($sp)
lw $ra -4($fp)
lw $fp 12($sp)
addu $sp $sp 20
j $ra
.text
.globl  Tree_SetHas_Right
Tree_SetHas_Right:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 20
sw $ra -4($fp)
sw $t7 0($sp)
sw $t8 4($sp)
sw $t9 8($sp)
move $t8 $a0
move $t9 $a1
sw $t9 20($t8)
li $t7 1
move $v0 $t7
lw $t7 0($sp)
lw $t8 4($sp)
lw $t9 8($sp)
lw $ra -4($fp)
lw $fp 12($sp)
addu $sp $sp 20
j $ra
.text
.globl  Tree_Compare
Tree_Compare:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 80
sw $ra -4($fp)
sw $s3 0($sp)
sw $t4 4($sp)
sw $t5 8($sp)
sw $s4 12($sp)
sw $t6 16($sp)
sw $s5 20($sp)
sw $t7 24($sp)
sw $s6 28($sp)
sw $t8 32($sp)
sw $s7 36($sp)
sw $t9 40($sp)
sw $t0 44($sp)
sw $t1 48($sp)
sw $s0 52($sp)
sw $t2 56($sp)
sw $s1 60($sp)
sw $t3 64($sp)
sw $s2 68($sp)
move $t8 $a1
move $t9 $a2
li $t7 0
move $s6 $t7
li $t6 1
add $t5 $t9 $t6
move $t4 $t5
li $t3 1
sub $t2 $t9 $t3
move $t1 $t2
sle $t0 $t8 $t1
beqz $t0 L6
li $s7 0
move $s6 $s7
b L7
L6: nop
li $s5 1
sub $s4 $t4 $s5
move $s3 $s4
li $s2 1
sle $s1 $t8 $s3
sub $s0 $s2 $s1
beqz $s0 L8
li $t7 0
move $s6 $t7
b L9
L8: nop
li $t6 1
move $s6 $t6
L9: nop
L7: nop
move $v0 $s6
lw $s3 0($sp)
lw $t4 4($sp)
lw $t5 8($sp)
lw $s4 12($sp)
lw $t6 16($sp)
lw $s5 20($sp)
lw $t7 24($sp)
lw $s6 28($sp)
lw $t8 32($sp)
lw $s7 36($sp)
lw $t9 40($sp)
lw $t0 44($sp)
lw $t1 48($sp)
lw $s0 52($sp)
lw $t2 56($sp)
lw $s1 60($sp)
lw $t3 64($sp)
lw $s2 68($sp)
lw $ra -4($fp)
lw $fp 72($sp)
addu $sp $sp 80
j $ra
.text
.globl  Tree_Insert
Tree_Insert:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 80
sw $ra -4($fp)
sw $s0 0($sp)
sw $s1 4($sp)
sw $s2 8($sp)
sw $s3 12($sp)
sw $s4 16($sp)
sw $s5 20($sp)
sw $s6 24($sp)
sw $s7 28($sp)
sw $t0 32($sp)
sw $t1 36($sp)
sw $t2 40($sp)
sw $t3 44($sp)
sw $t4 48($sp)
sw $t5 52($sp)
sw $t6 56($sp)
sw $t7 60($sp)
sw $t8 64($sp)
sw $t9 68($sp)
move $t8 $a0
move $t9 $a1
li $t7 84
move $a0 $t7
jal _halloc
move $t6 $v0
move $t5 $t6
li $t4 28
move $a0 $t4
jal _halloc
move $t3 $v0
move $t2 $t3
la $t1 Tree_accept
sw $t1 80($t5)
la $t0 Tree_RecPrint
sw $t0 76($t5)
la $s7 Tree_Print
sw $s7 72($t5)
la $s6 Tree_Search
sw $s6 68($t5)
la $s5 Tree_RemoveLeft
sw $s5 64($t5)
la $s4 Tree_RemoveRight
sw $s4 60($t5)
la $s3 Tree_Remove
sw $s3 56($t5)
la $s2 Tree_Delete
sw $s2 52($t5)
la $s1 Tree_Insert
sw $s1 48($t5)
la $s0 Tree_Compare
sw $s0 44($t5)
la $t7 Tree_SetHas_Right
sw $t7 40($t5)
la $t6 Tree_SetHas_Left
sw $t6 36($t5)
la $t4 Tree_GetHas_Left
sw $t4 32($t5)
la $t3 Tree_GetHas_Right
sw $t3 28($t5)
la $t1 Tree_SetKey
sw $t1 24($t5)
la $t0 Tree_GetKey
sw $t0 20($t5)
la $s7 Tree_GetLeft
sw $s7 16($t5)
la $s6 Tree_GetRight
sw $s6 12($t5)
la $s5 Tree_SetLeft
sw $s5 8($t5)
la $s4 Tree_SetRight
sw $s4 4($t5)
la $s3 Tree_Init
sw $s3 0($t5)
li $s2 4
move $s1 $s2
L10: nop
li $s0 27
sle $t7 $s1 $s0
beqz $t7 L11
add $t6 $t2 $s1
li $t4 0
sw $t4 0($t6)
li $t3 4
add $t1 $s1 $t3
move $s1 $t1
b L10
L11: nop
sw $t5 0($t2)
move $t0 $t2
move $s7 $t0
lw $s6 0($s7)
lw $s5 0($s6)
move $a0 $s7
move $a1 $t9
jalr $s5
move $s4 $v0
move $s3 $t8
li $s2 1
move $s0 $s2
L12: nop
beqz $s0 L13
move $t7 $s3
lw $t6 0($t7)
lw $t4 20($t6)
move $a0 $t7
jalr $t4
move $t3 $v0
move $s1 $t3
li $t2 1
sub $t1 $s1 $t2
move $t5 $t1
sle $s6 $t9 $t5
beqz $s6 L14
move $s7 $s3
lw $s5 0($s7)
lw $t8 32($s5)
move $a0 $s7
jalr $t8
move $s4 $v0
beqz $s4 L16
move $s2 $s3
lw $t6 0($s2)
lw $t7 16($t6)
move $a0 $s2
jalr $t7
move $t4 $v0
move $s3 $t4
b L17
L16: nop
li $t3 0
move $s0 $t3
move $s1 $s3
lw $t2 0($s1)
lw $t1 36($t2)
li $t5 1
move $a0 $s1
move $a1 $t5
jalr $t1
move $s6 $v0
move $s5 $s3
lw $s7 0($s5)
lw $t8 8($s7)
move $a0 $s5
move $a1 $t0
jalr $t8
move $s4 $v0
L17: nop
b L15
L14: nop
move $t6 $s3
lw $s2 0($t6)
lw $t7 28($s2)
move $a0 $t6
jalr $t7
move $t4 $v0
beqz $t4 L18
move $t3 $s3
lw $t2 0($t3)
lw $s1 12($t2)
move $a0 $t3
jalr $s1
move $t5 $v0
move $s3 $t5
b L19
L18: nop
li $t1 0
move $s0 $t1
move $s6 $s3
lw $s7 0($s6)
lw $s5 40($s7)
li $t8 1
move $a0 $s6
move $a1 $t8
jalr $s5
move $s4 $v0
move $s2 $s3
lw $t6 0($s2)
lw $t7 4($t6)
move $a0 $s2
move $a1 $t0
jalr $t7
move $t4 $v0
L19: nop
L15: nop
b L12
L13: nop
li $t2 1
move $v0 $t2
lw $s0 0($sp)
lw $s1 4($sp)
lw $s2 8($sp)
lw $s3 12($sp)
lw $s4 16($sp)
lw $s5 20($sp)
lw $s6 24($sp)
lw $s7 28($sp)
lw $t0 32($sp)
lw $t1 36($sp)
lw $t2 40($sp)
lw $t3 44($sp)
lw $t4 48($sp)
lw $t5 52($sp)
lw $t6 56($sp)
lw $t7 60($sp)
lw $t8 64($sp)
lw $t9 68($sp)
lw $ra -4($fp)
lw $fp 72($sp)
addu $sp $sp 80
j $ra
.text
.globl  Tree_Delete
Tree_Delete:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 80
sw $ra -4($fp)
sw $s0 0($sp)
sw $s1 4($sp)
sw $s2 8($sp)
sw $s3 12($sp)
sw $s4 16($sp)
sw $s5 20($sp)
sw $s6 24($sp)
sw $s7 28($sp)
sw $t0 32($sp)
sw $t1 36($sp)
sw $t2 40($sp)
sw $t3 44($sp)
sw $t4 48($sp)
sw $t5 52($sp)
sw $t6 56($sp)
sw $t7 60($sp)
sw $t8 64($sp)
sw $t9 68($sp)
move $t8 $a0
move $t9 $a1
move $t7 $t8
move $t6 $t8
li $t5 1
move $t4 $t5
li $t3 0
move $t2 $t3
li $t1 1
move $t0 $t1
L20: nop
beqz $t4 L21
move $s7 $t7
lw $s6 0($s7)
lw $s5 20($s6)
move $a0 $s7
jalr $s5
move $s4 $v0
move $s3 $s4
li $s2 1
sub $s1 $s3 $s2
move $s0 $s1
sle $t5 $t9 $s0
beqz $t5 L22
move $t3 $t7
lw $t1 0($t3)
lw $s6 32($t1)
move $a0 $t3
jalr $s6
move $s7 $v0
beqz $s7 L24
move $t6 $t7
move $s5 $t7
lw $s4 0($s5)
lw $s2 16($s4)
move $a0 $s5
jalr $s2
move $s1 $v0
move $t7 $s1
b L25
L24: nop
li $s0 0
move $t4 $s0
L25: nop
b L23
L22: nop
li $t5 1
sub $t1 $t9 $t5
move $t3 $t1
sle $s6 $s3 $t3
beqz $s6 L26
move $s7 $t7
lw $s4 0($s7)
lw $s5 28($s4)
move $a0 $s7
jalr $s5
move $s2 $v0
beqz $s2 L28
move $t6 $t7
move $s1 $t7
lw $s0 0($s1)
lw $t5 12($s0)
move $a0 $s1
jalr $t5
move $t1 $v0
move $t7 $t1
b L29
L28: nop
li $t3 0
move $t4 $t3
L29: nop
b L27
L26: nop
beqz $t0 L30
li $s3 0
move $s6 $s3
li $s4 1
move $s7 $t7
lw $s5 0($s7)
lw $s2 28($s5)
move $a0 $s7
jalr $s2
move $s0 $v0
sub $s1 $s4 $s0
beqz $s1 L34
li $t5 1
move $t1 $t7
lw $t3 0($t1)
lw $s3 32($t3)
move $a0 $t1
jalr $s3
move $s5 $v0
sub $s7 $t5 $s5
beqz $s7 L34
li $s2 1
move $s6 $s2
L34: nop
beqz $s6 L32
li $s0 1
b L33
L32: nop
move $s4 $t8
lw $s1 0($s4)
lw $t3 56($s1)
move $a0 $s4
move $a1 $t6
move $a2 $t7
jalr $t3
move $t1 $v0
L33: nop
b L31
L30: nop
move $s3 $t8
lw $s5 0($s3)
lw $t5 56($s5)
move $a0 $s3
move $a1 $t6
move $a2 $t7
jalr $t5
move $s7 $v0
L31: nop
li $s6 1
move $t2 $s6
li $s2 0
move $t4 $s2
L27: nop
L23: nop
li $s0 0
move $t0 $s0
b L20
L21: nop
move $v0 $t2
lw $s0 0($sp)
lw $s1 4($sp)
lw $s2 8($sp)
lw $s3 12($sp)
lw $s4 16($sp)
lw $s5 20($sp)
lw $s6 24($sp)
lw $s7 28($sp)
lw $t0 32($sp)
lw $t1 36($sp)
lw $t2 40($sp)
lw $t3 44($sp)
lw $t4 48($sp)
lw $t5 52($sp)
lw $t6 56($sp)
lw $t7 60($sp)
lw $t8 64($sp)
lw $t9 68($sp)
lw $ra -4($fp)
lw $fp 72($sp)
addu $sp $sp 80
j $ra
.text
.globl  Tree_Remove
Tree_Remove:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 80
sw $ra -4($fp)
sw $s0 0($sp)
sw $s1 4($sp)
sw $s2 8($sp)
sw $s3 12($sp)
sw $s4 16($sp)
sw $s5 20($sp)
sw $s6 24($sp)
sw $s7 28($sp)
sw $t0 32($sp)
sw $t1 36($sp)
sw $t2 40($sp)
sw $t3 44($sp)
sw $t4 48($sp)
sw $t5 52($sp)
sw $t6 56($sp)
sw $t7 60($sp)
sw $t8 64($sp)
sw $t9 68($sp)
move $t7 $a0
move $t8 $a1
move $t9 $a2
move $t6 $t9
lw $t5 0($t6)
lw $t4 32($t5)
move $a0 $t6
jalr $t4
move $t3 $v0
beqz $t3 L35
move $t2 $t7
lw $t1 0($t2)
lw $t0 64($t1)
move $a0 $t2
move $a1 $t8
move $a2 $t9
jalr $t0
move $s7 $v0
b L36
L35: nop
move $s6 $t9
lw $s5 0($s6)
lw $s4 28($s5)
move $a0 $s6
jalr $s4
move $s3 $v0
beqz $s3 L37
move $s2 $t7
lw $s1 0($s2)
lw $s0 60($s1)
move $a0 $s2
move $a1 $t8
move $a2 $t9
jalr $s0
move $t5 $v0
b L38
L37: nop
move $t6 $t9
lw $t4 0($t6)
lw $t3 20($t4)
move $a0 $t6
jalr $t3
move $t1 $v0
move $t2 $t1
move $t0 $t8
lw $s7 0($t0)
lw $s5 16($s7)
move $a0 $t0
jalr $s5
move $s6 $v0
move $s4 $s6
lw $s3 0($s4)
lw $s1 20($s3)
move $a0 $s4
jalr $s1
move $s2 $v0
move $s0 $s2
move $t9 $t7
lw $t5 0($t9)
lw $t4 44($t5)
move $a0 $t9
move $a1 $t2
move $a2 $s0
jalr $t4
move $t6 $v0
beqz $t6 L39
move $t3 $t8
lw $t1 0($t3)
lw $s7 8($t1)
lw $t0 24($t7)
move $a0 $t3
move $a1 $t0
jalr $s7
move $s5 $v0
move $s6 $t8
lw $s3 0($s6)
lw $s4 36($s3)
li $s1 0
move $a0 $s6
move $a1 $s1
jalr $s4
move $s2 $v0
b L40
L39: nop
move $t5 $t8
lw $t2 0($t5)
lw $t9 4($t2)
lw $s0 24($t7)
move $a0 $t5
move $a1 $s0
jalr $t9
move $t6 $v0
move $t4 $t8
lw $t1 0($t4)
lw $t3 40($t1)
li $t0 0
move $a0 $t4
move $a1 $t0
jalr $t3
move $s7 $v0
L40: nop
L38: nop
L36: nop
li $s5 1
move $v0 $s5
lw $s0 0($sp)
lw $s1 4($sp)
lw $s2 8($sp)
lw $s3 12($sp)
lw $s4 16($sp)
lw $s5 20($sp)
lw $s6 24($sp)
lw $s7 28($sp)
lw $t0 32($sp)
lw $t1 36($sp)
lw $t2 40($sp)
lw $t3 44($sp)
lw $t4 48($sp)
lw $t5 52($sp)
lw $t6 56($sp)
lw $t7 60($sp)
lw $t8 64($sp)
lw $t9 68($sp)
lw $ra -4($fp)
lw $fp 72($sp)
addu $sp $sp 80
j $ra
.text
.globl  Tree_RemoveRight
Tree_RemoveRight:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 80
sw $ra -4($fp)
sw $t4 0($sp)
sw $t5 4($sp)
sw $t6 8($sp)
sw $t7 12($sp)
sw $t8 16($sp)
sw $t9 20($sp)
sw $s0 24($sp)
sw $s1 28($sp)
sw $s2 32($sp)
sw $s3 36($sp)
sw $s4 40($sp)
sw $s5 44($sp)
sw $s6 48($sp)
sw $s7 52($sp)
sw $t0 56($sp)
sw $t1 60($sp)
sw $t2 64($sp)
sw $t3 68($sp)
move $t7 $a0
move $t8 $a1
move $t9 $a2
L41: nop
move $t6 $t9
lw $t5 0($t6)
lw $t4 28($t5)
move $a0 $t6
jalr $t4
move $t3 $v0
beqz $t3 L42
move $t2 $t9
lw $t1 0($t2)
lw $t0 24($t1)
move $s7 $t9
lw $s6 0($s7)
lw $s5 12($s6)
move $a0 $s7
jalr $s5
move $s4 $v0
move $s3 $s4
lw $s2 0($s3)
lw $s1 20($s2)
move $a0 $s3
jalr $s1
move $s0 $v0
move $a0 $t2
move $a1 $s0
jalr $t0
move $t5 $v0
move $t8 $t9
move $t6 $t9
lw $t4 0($t6)
lw $t3 12($t4)
move $a0 $t6
jalr $t3
move $t1 $v0
move $t9 $t1
b L41
L42: nop
move $s6 $t8
lw $s7 0($s6)
lw $s5 4($s7)
lw $s4 24($t7)
move $a0 $s6
move $a1 $s4
jalr $s5
move $s2 $v0
move $s3 $t8
lw $s1 0($s3)
lw $t0 40($s1)
li $t2 0
move $a0 $s3
move $a1 $t2
jalr $t0
move $t5 $v0
li $s0 1
move $v0 $s0
lw $t4 0($sp)
lw $t5 4($sp)
lw $t6 8($sp)
lw $t7 12($sp)
lw $t8 16($sp)
lw $t9 20($sp)
lw $s0 24($sp)
lw $s1 28($sp)
lw $s2 32($sp)
lw $s3 36($sp)
lw $s4 40($sp)
lw $s5 44($sp)
lw $s6 48($sp)
lw $s7 52($sp)
lw $t0 56($sp)
lw $t1 60($sp)
lw $t2 64($sp)
lw $t3 68($sp)
lw $ra -4($fp)
lw $fp 72($sp)
addu $sp $sp 80
j $ra
.text
.globl  Tree_RemoveLeft
Tree_RemoveLeft:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 80
sw $ra -4($fp)
sw $t4 0($sp)
sw $t5 4($sp)
sw $t6 8($sp)
sw $t7 12($sp)
sw $t8 16($sp)
sw $t9 20($sp)
sw $s0 24($sp)
sw $s1 28($sp)
sw $s2 32($sp)
sw $s3 36($sp)
sw $s4 40($sp)
sw $s5 44($sp)
sw $s6 48($sp)
sw $s7 52($sp)
sw $t0 56($sp)
sw $t1 60($sp)
sw $t2 64($sp)
sw $t3 68($sp)
move $t7 $a0
move $t8 $a1
move $t9 $a2
L43: nop
move $t6 $t9
lw $t5 0($t6)
lw $t4 32($t5)
move $a0 $t6
jalr $t4
move $t3 $v0
beqz $t3 L44
move $t2 $t9
lw $t1 0($t2)
lw $t0 24($t1)
move $s7 $t9
lw $s6 0($s7)
lw $s5 16($s6)
move $a0 $s7
jalr $s5
move $s4 $v0
move $s3 $s4
lw $s2 0($s3)
lw $s1 20($s2)
move $a0 $s3
jalr $s1
move $s0 $v0
move $a0 $t2
move $a1 $s0
jalr $t0
move $t5 $v0
move $t8 $t9
move $t6 $t9
lw $t4 0($t6)
lw $t3 16($t4)
move $a0 $t6
jalr $t3
move $t1 $v0
move $t9 $t1
b L43
L44: nop
move $s6 $t8
lw $s7 0($s6)
lw $s5 8($s7)
lw $s4 24($t7)
move $a0 $s6
move $a1 $s4
jalr $s5
move $s2 $v0
move $s3 $t8
lw $s1 0($s3)
lw $t0 36($s1)
li $t2 0
move $a0 $s3
move $a1 $t2
jalr $t0
move $t5 $v0
li $s0 1
move $v0 $s0
lw $t4 0($sp)
lw $t5 4($sp)
lw $t6 8($sp)
lw $t7 12($sp)
lw $t8 16($sp)
lw $t9 20($sp)
lw $s0 24($sp)
lw $s1 28($sp)
lw $s2 32($sp)
lw $s3 36($sp)
lw $s4 40($sp)
lw $s5 44($sp)
lw $s6 48($sp)
lw $s7 52($sp)
lw $t0 56($sp)
lw $t1 60($sp)
lw $t2 64($sp)
lw $t3 68($sp)
lw $ra -4($fp)
lw $fp 72($sp)
addu $sp $sp 80
j $ra
.text
.globl  Tree_Search
Tree_Search:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 80
sw $ra -4($fp)
sw $t4 0($sp)
sw $t5 4($sp)
sw $t6 8($sp)
sw $t7 12($sp)
sw $t8 16($sp)
sw $t9 20($sp)
sw $s0 24($sp)
sw $s1 28($sp)
sw $s2 32($sp)
sw $s3 36($sp)
sw $s4 40($sp)
sw $s5 44($sp)
sw $s6 48($sp)
sw $s7 52($sp)
sw $t0 56($sp)
sw $t1 60($sp)
sw $t2 64($sp)
sw $t3 68($sp)
move $t8 $a0
move $t9 $a1
move $t7 $t8
li $t6 1
move $t5 $t6
li $t4 0
move $t3 $t4
L45: nop
beqz $t5 L46
move $t2 $t7
lw $t1 0($t2)
lw $t0 20($t1)
move $a0 $t2
jalr $t0
move $s7 $v0
move $s6 $s7
li $s5 1
sub $s4 $s6 $s5
move $s3 $s4
sle $s2 $t9 $s3
beqz $s2 L47
move $s1 $t7
lw $s0 0($s1)
lw $t8 32($s0)
move $a0 $s1
jalr $t8
move $t6 $v0
beqz $t6 L49
move $t4 $t7
lw $t1 0($t4)
lw $t2 16($t1)
move $a0 $t4
jalr $t2
move $t0 $v0
move $t7 $t0
b L50
L49: nop
li $s7 0
move $t5 $s7
L50: nop
b L48
L47: nop
li $s5 1
sub $s4 $t9 $s5
move $s3 $s4
sle $s2 $s6 $s3
beqz $s2 L51
move $s0 $t7
lw $s1 0($s0)
lw $t8 28($s1)
move $a0 $s0
jalr $t8
move $t6 $v0
beqz $t6 L53
move $t1 $t7
lw $t4 0($t1)
lw $t2 12($t4)
move $a0 $t1
jalr $t2
move $t0 $v0
move $t7 $t0
b L54
L53: nop
li $s7 0
move $t5 $s7
L54: nop
b L52
L51: nop
li $s5 1
move $t3 $s5
li $s4 0
move $t5 $s4
L52: nop
L48: nop
b L45
L46: nop
move $v0 $t3
lw $t4 0($sp)
lw $t5 4($sp)
lw $t6 8($sp)
lw $t7 12($sp)
lw $t8 16($sp)
lw $t9 20($sp)
lw $s0 24($sp)
lw $s1 28($sp)
lw $s2 32($sp)
lw $s3 36($sp)
lw $s4 40($sp)
lw $s5 44($sp)
lw $s6 48($sp)
lw $s7 52($sp)
lw $t0 56($sp)
lw $t1 60($sp)
lw $t2 64($sp)
lw $t3 68($sp)
lw $ra -4($fp)
lw $fp 72($sp)
addu $sp $sp 80
j $ra
.text
.globl  Tree_Print
Tree_Print:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 36
sw $ra -4($fp)
sw $t4 0($sp)
sw $t5 4($sp)
sw $t6 8($sp)
sw $t7 12($sp)
sw $t8 16($sp)
sw $t9 20($sp)
sw $t3 24($sp)
move $t9 $a0
move $t8 $t9
move $t7 $t9
lw $t6 0($t7)
lw $t5 76($t6)
move $a0 $t7
move $a1 $t8
jalr $t5
move $t4 $v0
li $t3 1
move $v0 $t3
lw $t4 0($sp)
lw $t5 4($sp)
lw $t6 8($sp)
lw $t7 12($sp)
lw $t8 16($sp)
lw $t9 20($sp)
lw $t3 24($sp)
lw $ra -4($fp)
lw $fp 28($sp)
addu $sp $sp 36
j $ra
.text
.globl  Tree_RecPrint
Tree_RecPrint:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 80
sw $ra -4($fp)
sw $t4 0($sp)
sw $t5 4($sp)
sw $t6 8($sp)
sw $t7 12($sp)
sw $t8 16($sp)
sw $t9 20($sp)
sw $s0 24($sp)
sw $s1 28($sp)
sw $s2 32($sp)
sw $s3 36($sp)
sw $s4 40($sp)
sw $s5 44($sp)
sw $s6 48($sp)
sw $s7 52($sp)
sw $t0 56($sp)
sw $t1 60($sp)
sw $t2 64($sp)
sw $t3 68($sp)
move $t8 $a0
move $t9 $a1
move $t7 $t9
lw $t6 0($t7)
lw $t5 32($t6)
move $a0 $t7
jalr $t5
move $t4 $v0
beqz $t4 L55
move $t3 $t8
lw $t2 0($t3)
lw $t1 76($t2)
move $t0 $t9
lw $s7 0($t0)
lw $s6 16($s7)
move $a0 $t0
jalr $s6
move $s5 $v0
move $a0 $t3
move $a1 $s5
jalr $t1
move $s4 $v0
b L56
L55: nop
li $s3 1
L56: nop
move $s2 $t9
lw $s1 0($s2)
lw $s0 20($s1)
move $a0 $s2
jalr $s0
move $t6 $v0
move $a0 $t6
jal _print
move $t7 $t9
lw $t5 0($t7)
lw $t4 28($t5)
move $a0 $t7
jalr $t4
move $t2 $v0
beqz $t2 L57
move $s7 $t8
lw $t0 0($s7)
lw $s6 76($t0)
move $t1 $t9
lw $t3 0($t1)
lw $s4 12($t3)
move $a0 $t1
jalr $s4
move $s5 $v0
move $a0 $s7
move $a1 $s5
jalr $s6
move $s3 $v0
b L58
L57: nop
li $s1 1
L58: nop
li $s2 1
move $v0 $s2
lw $t4 0($sp)
lw $t5 4($sp)
lw $t6 8($sp)
lw $t7 12($sp)
lw $t8 16($sp)
lw $t9 20($sp)
lw $s0 24($sp)
lw $s1 28($sp)
lw $s2 32($sp)
lw $s3 36($sp)
lw $s4 40($sp)
lw $s5 44($sp)
lw $s6 48($sp)
lw $s7 52($sp)
lw $t0 56($sp)
lw $t1 60($sp)
lw $t2 64($sp)
lw $t3 68($sp)
lw $ra -4($fp)
lw $fp 72($sp)
addu $sp $sp 80
j $ra
.text
.globl  Tree_accept
Tree_accept:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 40
sw $ra -4($fp)
sw $t4 0($sp)
sw $t5 4($sp)
sw $t6 8($sp)
sw $t7 12($sp)
sw $t8 16($sp)
sw $t9 20($sp)
sw $t2 24($sp)
sw $t3 28($sp)
move $t8 $a0
move $t9 $a1
li $t7 333
move $a0 $t7
jal _print
move $t6 $t9
lw $t5 0($t6)
lw $t4 0($t5)
move $a0 $t6
move $a1 $t8
jalr $t4
move $t3 $v0
li $t2 0
move $v0 $t2
lw $t4 0($sp)
lw $t5 4($sp)
lw $t6 8($sp)
lw $t7 12($sp)
lw $t8 16($sp)
lw $t9 20($sp)
lw $t2 24($sp)
lw $t3 28($sp)
lw $ra -4($fp)
lw $fp 32($sp)
addu $sp $sp 40
j $ra
.text
.globl  Visitor_visit
Visitor_visit:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 80
sw $ra -4($fp)
sw $t4 0($sp)
sw $t5 4($sp)
sw $t6 8($sp)
sw $t7 12($sp)
sw $t8 16($sp)
sw $t9 20($sp)
sw $s0 24($sp)
sw $s1 28($sp)
sw $s2 32($sp)
sw $s3 36($sp)
sw $s4 40($sp)
sw $s5 44($sp)
sw $s6 48($sp)
sw $s7 52($sp)
sw $t0 56($sp)
sw $t1 60($sp)
sw $t2 64($sp)
sw $t3 68($sp)
move $t8 $a0
move $t9 $a1
move $t7 $t9
lw $t6 0($t7)
lw $t5 28($t6)
move $a0 $t7
jalr $t5
move $t4 $v0
beqz $t4 L59
move $t3 $t9
lw $t2 0($t3)
lw $t1 12($t2)
move $a0 $t3
jalr $t1
move $t0 $v0
sw $t0 8($t8)
lw $s7 8($t8)
move $s6 $s7
lw $s5 0($s6)
lw $s4 80($s5)
move $a0 $s6
move $a1 $t8
jalr $s4
move $s3 $v0
b L60
L59: nop
li $s2 0
L60: nop
move $s1 $t9
lw $s0 0($s1)
lw $t6 32($s0)
move $a0 $s1
jalr $t6
move $t7 $v0
beqz $t7 L61
move $t5 $t9
lw $t4 0($t5)
lw $t2 16($t4)
move $a0 $t5
jalr $t2
move $t3 $v0
sw $t3 4($t8)
lw $t1 4($t8)
move $t0 $t1
lw $s7 0($t0)
lw $s5 80($s7)
move $a0 $t0
move $a1 $t8
jalr $s5
move $s6 $v0
b L62
L61: nop
li $s4 0
L62: nop
li $s3 0
move $v0 $s3
lw $t4 0($sp)
lw $t5 4($sp)
lw $t6 8($sp)
lw $t7 12($sp)
lw $t8 16($sp)
lw $t9 20($sp)
lw $s0 24($sp)
lw $s1 28($sp)
lw $s2 32($sp)
lw $s3 36($sp)
lw $s4 40($sp)
lw $s5 44($sp)
lw $s6 48($sp)
lw $s7 52($sp)
lw $t0 56($sp)
lw $t1 60($sp)
lw $t2 64($sp)
lw $t3 68($sp)
lw $ra -4($fp)
lw $fp 72($sp)
addu $sp $sp 80
j $ra
.text
.globl  MyVisitor_visit
MyVisitor_visit:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 80
sw $ra -4($fp)
sw $t4 0($sp)
sw $t5 4($sp)
sw $t6 8($sp)
sw $t7 12($sp)
sw $t8 16($sp)
sw $t9 20($sp)
sw $s0 24($sp)
sw $s1 28($sp)
sw $s2 32($sp)
sw $s3 36($sp)
sw $s4 40($sp)
sw $s5 44($sp)
sw $s6 48($sp)
sw $s7 52($sp)
sw $t0 56($sp)
sw $t1 60($sp)
sw $t2 64($sp)
sw $t3 68($sp)
move $t8 $a0
move $t9 $a1
move $t7 $t9
lw $t6 0($t7)
lw $t5 28($t6)
move $a0 $t7
jalr $t5
move $t4 $v0
beqz $t4 L63
move $t3 $t9
lw $t2 0($t3)
lw $t1 12($t2)
move $a0 $t3
jalr $t1
move $t0 $v0
sw $t0 8($t8)
lw $s7 8($t8)
move $s6 $s7
lw $s5 0($s6)
lw $s4 80($s5)
move $a0 $s6
move $a1 $t8
jalr $s4
move $s3 $v0
b L64
L63: nop
li $s2 0
L64: nop
move $s1 $t9
lw $s0 0($s1)
lw $t6 20($s0)
move $a0 $s1
jalr $t6
move $t7 $v0
move $a0 $t7
jal _print
move $t5 $t9
lw $t4 0($t5)
lw $t2 32($t4)
move $a0 $t5
jalr $t2
move $t3 $v0
beqz $t3 L65
move $t1 $t9
lw $t0 0($t1)
lw $s7 16($t0)
move $a0 $t1
jalr $s7
move $s5 $v0
sw $s5 4($t8)
lw $s6 4($t8)
move $s4 $s6
lw $s3 0($s4)
lw $s2 80($s3)
move $a0 $s4
move $a1 $t8
jalr $s2
move $s0 $v0
b L66
L65: nop
li $s1 0
L66: nop
li $t6 0
move $v0 $t6
lw $t4 0($sp)
lw $t5 4($sp)
lw $t6 8($sp)
lw $t7 12($sp)
lw $t8 16($sp)
lw $t9 20($sp)
lw $s0 24($sp)
lw $s1 28($sp)
lw $s2 32($sp)
lw $s3 36($sp)
lw $s4 40($sp)
lw $s5 44($sp)
lw $s6 48($sp)
lw $s7 52($sp)
lw $t0 56($sp)
lw $t1 60($sp)
lw $t2 64($sp)
lw $t3 68($sp)
lw $ra -4($fp)
lw $fp 72($sp)
addu $sp $sp 80
j $ra
.text
         .globl _halloc
_halloc:
         li $v0, 9
         syscall
         j $ra

         .text
         .globl _print
_print:
         li $v0, 1
         syscall
         la $a0, newl
         li $v0, 4
         syscall
         j $ra

         .data
         .align   0
newl:    .asciiz "\n" 
         .data
         .align   0
str_er:  .asciiz " ERROR: abnormal termination\n" 
