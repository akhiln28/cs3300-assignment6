.text
.globl  main
main:
move $fp $sp
subu $sp $sp 48
sw $ra -4($fp)
li $t9 4
move $a0 $t9
jal _halloc
move $t8 $v0
move $t7 $t8
li $t6 4
move $a0 $t6
jal _halloc
move $t5 $v0
move $t4 $t5
la $t3 LL_Start
sw $t3 0($t7)
sw $t7 0($t4)
move $t2 $t4
lw $t1 0($t2)
lw $t0 0($t1)
move $a0 $t2
jalr $t0
move $s7 $v0
move $a0 $s7
jal _print
lw $ra -4($fp)
addu $sp $sp 48
j $ra
.text
.globl  Element_Init
Element_Init:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 28
sw $ra -4($fp)
sw $t5 0($sp)
sw $t6 4($sp)
sw $t7 8($sp)
sw $t8 12($sp)
sw $t9 16($sp)
move $t6 $a0
move $t7 $a1
move $t8 $a2
move $t9 $a3
sw $t7 4($t6)
sw $t8 8($t6)
sw $t9 12($t6)
li $t5 1
move $v0 $t5
lw $t5 0($sp)
lw $t6 4($sp)
lw $t7 8($sp)
lw $t8 12($sp)
lw $t9 16($sp)
lw $ra -4($fp)
lw $fp 20($sp)
addu $sp $sp 28
j $ra
.text
.globl  Element_GetAge
Element_GetAge:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 16
sw $ra -4($fp)
sw $t8 0($sp)
sw $t9 4($sp)
move $t9 $a0
lw $t8 4($t9)
move $v0 $t8
lw $t8 0($sp)
lw $t9 4($sp)
lw $ra -4($fp)
lw $fp 8($sp)
addu $sp $sp 16
j $ra
.text
.globl  Element_GetSalary
Element_GetSalary:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 16
sw $ra -4($fp)
sw $t8 0($sp)
sw $t9 4($sp)
move $t9 $a0
lw $t8 8($t9)
move $v0 $t8
lw $t8 0($sp)
lw $t9 4($sp)
lw $ra -4($fp)
lw $fp 8($sp)
addu $sp $sp 16
j $ra
.text
.globl  Element_GetMarried
Element_GetMarried:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 16
sw $ra -4($fp)
sw $t8 0($sp)
sw $t9 4($sp)
move $t9 $a0
lw $t8 12($t9)
move $v0 $t8
lw $t8 0($sp)
lw $t9 4($sp)
lw $ra -4($fp)
lw $fp 8($sp)
addu $sp $sp 16
j $ra
.text
.globl  Element_Equal
Element_Equal:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 80
sw $ra -4($fp)
sw $t4 0($sp)
sw $t5 4($sp)
sw $t6 8($sp)
sw $t7 12($sp)
sw $t8 16($sp)
sw $t9 20($sp)
sw $s0 24($sp)
sw $s1 28($sp)
sw $s2 32($sp)
sw $s3 36($sp)
sw $s4 40($sp)
sw $s5 44($sp)
sw $s6 48($sp)
sw $s7 52($sp)
sw $t0 56($sp)
sw $t1 60($sp)
sw $t2 64($sp)
sw $t3 68($sp)
move $t8 $a0
move $t9 $a1
li $t7 1
move $t6 $t7
move $t5 $t9
lw $t4 0($t5)
lw $t3 4($t4)
move $a0 $t5
jalr $t3
move $t2 $v0
move $t1 $t2
li $t0 1
move $s7 $t8
lw $s6 0($s7)
lw $s5 20($s6)
lw $s4 4($t8)
move $a0 $s7
move $a1 $t1
move $a2 $s4
jalr $s5
move $s3 $v0
sub $s2 $t0 $s3
beqz $s2 L2
li $s1 0
move $t6 $s1
b L3
L2: nop
move $s0 $t9
lw $t7 0($s0)
lw $t4 8($t7)
move $a0 $s0
jalr $t4
move $t5 $v0
move $t3 $t5
li $t2 1
move $s6 $t8
lw $t1 0($s6)
lw $s5 20($t1)
lw $s7 8($t8)
move $a0 $s6
move $a1 $t3
move $a2 $s7
jalr $s5
move $t0 $v0
sub $s4 $t2 $t0
beqz $s4 L4
li $s2 0
move $t6 $s2
b L5
L4: nop
lw $s3 12($t8)
beqz $s3 L6
li $s1 1
move $t7 $t9
lw $s0 0($t7)
lw $t4 12($s0)
move $a0 $t7
jalr $t4
move $t5 $v0
sub $t1 $s1 $t5
beqz $t1 L8
li $t3 0
move $t6 $t3
b L9
L8: nop
li $s5 0
L9: nop
b L7
L6: nop
move $s6 $t9
lw $t2 0($s6)
lw $s7 12($t2)
move $a0 $s6
jalr $s7
move $s4 $v0
beqz $s4 L10
li $t0 0
move $t6 $t0
b L11
L10: nop
li $t8 0
L11: nop
L7: nop
L5: nop
L3: nop
move $v0 $t6
lw $t4 0($sp)
lw $t5 4($sp)
lw $t6 8($sp)
lw $t7 12($sp)
lw $t8 16($sp)
lw $t9 20($sp)
lw $s0 24($sp)
lw $s1 28($sp)
lw $s2 32($sp)
lw $s3 36($sp)
lw $s4 40($sp)
lw $s5 44($sp)
lw $s6 48($sp)
lw $s7 52($sp)
lw $t0 56($sp)
lw $t1 60($sp)
lw $t2 64($sp)
lw $t3 68($sp)
lw $ra -4($fp)
lw $fp 72($sp)
addu $sp $sp 80
j $ra
.text
.globl  Element_Compare
Element_Compare:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 80
sw $ra -4($fp)
sw $t4 0($sp)
sw $s3 4($sp)
sw $t5 8($sp)
sw $s4 12($sp)
sw $s5 16($sp)
sw $t6 20($sp)
sw $s6 24($sp)
sw $t7 28($sp)
sw $t8 32($sp)
sw $s7 36($sp)
sw $t9 40($sp)
sw $t0 44($sp)
sw $t1 48($sp)
sw $s0 52($sp)
sw $t2 56($sp)
sw $s1 60($sp)
sw $t3 64($sp)
sw $s2 68($sp)
move $t8 $a1
move $t9 $a2
li $t7 0
move $s7 $t7
li $t6 1
add $t5 $t9 $t6
move $t4 $t5
li $t3 1
sub $t2 $t9 $t3
sle $t1 $t8 $t2
beqz $t1 L12
li $t0 0
move $s7 $t0
b L13
L12: nop
li $s6 1
li $s5 1
sub $s4 $t4 $s5
sle $s3 $t8 $s4
sub $s2 $s6 $s3
beqz $s2 L14
li $s1 0
move $s7 $s1
b L15
L14: nop
li $s0 1
move $s7 $s0
L15: nop
L13: nop
move $v0 $s7
lw $t4 0($sp)
lw $s3 4($sp)
lw $t5 8($sp)
lw $s4 12($sp)
lw $s5 16($sp)
lw $t6 20($sp)
lw $s6 24($sp)
lw $t7 28($sp)
lw $t8 32($sp)
lw $s7 36($sp)
lw $t9 40($sp)
lw $t0 44($sp)
lw $t1 48($sp)
lw $s0 52($sp)
lw $t2 56($sp)
lw $s1 60($sp)
lw $t3 64($sp)
lw $s2 68($sp)
lw $ra -4($fp)
lw $fp 72($sp)
addu $sp $sp 80
j $ra
.text
.globl  List_Init
List_Init:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 20
sw $ra -4($fp)
sw $t7 0($sp)
sw $t8 4($sp)
sw $t9 8($sp)
move $t9 $a0
li $t8 1
sw $t8 12($t9)
li $t7 1
move $v0 $t7
lw $t7 0($sp)
lw $t8 4($sp)
lw $t9 8($sp)
lw $ra -4($fp)
lw $fp 12($sp)
addu $sp $sp 20
j $ra
.text
.globl  List_InitNew
List_InitNew:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 28
sw $ra -4($fp)
sw $t5 0($sp)
sw $t6 4($sp)
sw $t7 8($sp)
sw $t8 12($sp)
sw $t9 16($sp)
move $t6 $a0
move $t7 $a1
move $t8 $a2
move $t9 $a3
sw $t9 12($t6)
sw $t7 4($t6)
sw $t8 8($t6)
li $t5 1
move $v0 $t5
lw $t5 0($sp)
lw $t6 4($sp)
lw $t7 8($sp)
lw $t8 12($sp)
lw $t9 16($sp)
lw $ra -4($fp)
lw $fp 20($sp)
addu $sp $sp 28
j $ra
.text
.globl  List_Insert
List_Insert:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 80
sw $ra -4($fp)
sw $t4 0($sp)
sw $t5 4($sp)
sw $t6 8($sp)
sw $t7 12($sp)
sw $t8 16($sp)
sw $t9 20($sp)
sw $s0 24($sp)
sw $s1 28($sp)
sw $s2 32($sp)
sw $s3 36($sp)
sw $s4 40($sp)
sw $s5 44($sp)
sw $s6 48($sp)
sw $s7 52($sp)
sw $t0 56($sp)
sw $t1 60($sp)
sw $t2 64($sp)
sw $t3 68($sp)
move $t8 $a0
move $t9 $a1
move $t7 $t8
li $t6 40
move $a0 $t6
jal _halloc
move $t5 $v0
move $t4 $t5
li $t3 16
move $a0 $t3
jal _halloc
move $t2 $v0
move $t1 $t2
la $t0 List_Print
sw $t0 36($t4)
la $s7 List_GetNext
sw $s7 32($t4)
la $s6 List_GetElem
sw $s6 28($t4)
la $s5 List_GetEnd
sw $s5 24($t4)
la $s4 List_Search
sw $s4 20($t4)
la $s3 List_Delete
sw $s3 16($t4)
la $s2 List_SetNext
sw $s2 12($t4)
la $s1 List_Insert
sw $s1 8($t4)
la $s0 List_InitNew
sw $s0 4($t4)
la $t8 List_Init
sw $t8 0($t4)
li $t6 4
move $t5 $t6
L16: nop
li $t3 15
sle $t2 $t5 $t3
beqz $t2 L17
add $t0 $t1 $t5
li $s7 0
sw $s7 0($t0)
li $s6 4
add $s5 $t5 $s6
move $t5 $s5
b L16
L17: nop
sw $t4 0($t1)
move $s4 $t1
move $s3 $s4
lw $s2 0($s3)
lw $s1 4($s2)
li $s0 0
move $a0 $s3
move $a1 $t9
move $a2 $t7
move $a3 $s0
jalr $s1
move $t8 $v0
move $v0 $s4
lw $t4 0($sp)
lw $t5 4($sp)
lw $t6 8($sp)
lw $t7 12($sp)
lw $t8 16($sp)
lw $t9 20($sp)
lw $s0 24($sp)
lw $s1 28($sp)
lw $s2 32($sp)
lw $s3 36($sp)
lw $s4 40($sp)
lw $s5 44($sp)
lw $s6 48($sp)
lw $s7 52($sp)
lw $t0 56($sp)
lw $t1 60($sp)
lw $t2 64($sp)
lw $t3 68($sp)
lw $ra -4($fp)
lw $fp 72($sp)
addu $sp $sp 80
j $ra
.text
.globl  List_SetNext
List_SetNext:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 20
sw $ra -4($fp)
sw $t7 0($sp)
sw $t8 4($sp)
sw $t9 8($sp)
move $t8 $a0
move $t9 $a1
sw $t9 8($t8)
li $t7 1
move $v0 $t7
lw $t7 0($sp)
lw $t8 4($sp)
lw $t9 8($sp)
lw $ra -4($fp)
lw $fp 12($sp)
addu $sp $sp 20
j $ra
.text
.globl  List_Delete
List_Delete:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 80
sw $ra -4($fp)
sw $s0 0($sp)
sw $s1 4($sp)
sw $s2 8($sp)
sw $s3 12($sp)
sw $s4 16($sp)
sw $s5 20($sp)
sw $s6 24($sp)
sw $s7 28($sp)
sw $t0 32($sp)
sw $t1 36($sp)
sw $t2 40($sp)
sw $t3 44($sp)
sw $t4 48($sp)
sw $t5 52($sp)
sw $t6 56($sp)
sw $t7 60($sp)
sw $t8 64($sp)
sw $t9 68($sp)
move $t8 $a0
move $t9 $a1
move $t7 $t8
li $t6 0
move $t5 $t6
li $t4 0
li $t3 1
sub $t2 $t4 $t3
move $t1 $t2
move $t0 $t8
move $s7 $t8
lw $s6 12($t8)
move $s5 $s6
lw $s4 4($t8)
move $s3 $s4
L18: nop
li $s2 0
move $s1 $s2
li $s0 1
sub $t6 $s0 $s5
beqz $t6 L20
li $t4 1
sub $t3 $t4 $t5
beqz $t3 L20
li $t2 1
move $s1 $t2
L20: nop
beqz $s1 L19
move $s6 $t9
lw $t8 0($s6)
lw $s4 16($t8)
move $a0 $s6
move $a1 $s3
jalr $s4
move $s2 $v0
beqz $s2 L21
li $s0 1
move $t5 $s0
li $t6 0
move $t4 $t6
li $t3 1
sub $t2 $t4 $t3
move $s1 $t2
sle $t8 $t1 $s1
beqz $t8 L23
move $s6 $t0
lw $s4 0($s6)
lw $s2 32($s4)
move $a0 $s6
jalr $s2
move $s0 $v0
move $t7 $s0
b L24
L23: nop
li $t6 0
li $t4 555
sub $t3 $t6 $t4
move $a0 $t3
jal _print
move $t2 $s7
lw $s1 0($t2)
lw $t8 12($s1)
move $s4 $t0
lw $s6 0($s4)
lw $s2 32($s6)
move $a0 $s4
jalr $s2
move $s0 $v0
move $a0 $t2
move $a1 $s0
jalr $t8
move $t6 $v0
li $t4 0
li $t3 555
sub $s1 $t4 $t3
move $a0 $s1
jal _print
L24: nop
b L22
L21: nop
li $s6 0
L22: nop
li $s4 1
sub $s2 $s4 $t5
beqz $s2 L25
move $s7 $t0
move $t8 $t0
lw $t2 0($t8)
lw $t6 32($t2)
move $a0 $t8
jalr $t6
move $s0 $v0
move $t0 $s0
move $t4 $t0
lw $t3 0($t4)
lw $s1 24($t3)
move $a0 $t4
jalr $s1
move $s6 $v0
move $s5 $s6
move $s4 $t0
lw $s2 0($s4)
lw $t2 28($s2)
move $a0 $s4
jalr $t2
move $t8 $v0
move $s3 $t8
li $t6 1
move $t1 $t6
b L26
L25: nop
li $s0 0
L26: nop
b L18
L19: nop
move $v0 $t7
lw $s0 0($sp)
lw $s1 4($sp)
lw $s2 8($sp)
lw $s3 12($sp)
lw $s4 16($sp)
lw $s5 20($sp)
lw $s6 24($sp)
lw $s7 28($sp)
lw $t0 32($sp)
lw $t1 36($sp)
lw $t2 40($sp)
lw $t3 44($sp)
lw $t4 48($sp)
lw $t5 52($sp)
lw $t6 56($sp)
lw $t7 60($sp)
lw $t8 64($sp)
lw $t9 68($sp)
lw $ra -4($fp)
lw $fp 72($sp)
addu $sp $sp 80
j $ra
.text
.globl  List_Search
List_Search:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 80
sw $ra -4($fp)
sw $t4 0($sp)
sw $t5 4($sp)
sw $t6 8($sp)
sw $t7 12($sp)
sw $t8 16($sp)
sw $t9 20($sp)
sw $s0 24($sp)
sw $s1 28($sp)
sw $s2 32($sp)
sw $s3 36($sp)
sw $s4 40($sp)
sw $s5 44($sp)
sw $s6 48($sp)
sw $s7 52($sp)
sw $t0 56($sp)
sw $t1 60($sp)
sw $t2 64($sp)
sw $t3 68($sp)
move $t8 $a0
move $t9 $a1
li $t7 0
move $t6 $t7
move $t5 $t8
lw $t4 12($t8)
move $t3 $t4
lw $t2 4($t8)
move $t1 $t2
L27: nop
li $t0 1
sub $s7 $t0 $t3
beqz $s7 L28
move $s6 $t9
lw $s5 0($s6)
lw $s4 16($s5)
move $a0 $s6
move $a1 $t1
jalr $s4
move $s3 $v0
beqz $s3 L29
li $s2 1
move $t6 $s2
b L30
L29: nop
li $s1 0
L30: nop
move $s0 $t5
lw $t7 0($s0)
lw $t4 32($t7)
move $a0 $s0
jalr $t4
move $t8 $v0
move $t5 $t8
move $t2 $t5
lw $t0 0($t2)
lw $s7 24($t0)
move $a0 $t2
jalr $s7
move $s5 $v0
move $t3 $s5
move $s6 $t5
lw $s4 0($s6)
lw $s3 28($s4)
move $a0 $s6
jalr $s3
move $s2 $v0
move $t1 $s2
b L27
L28: nop
move $v0 $t6
lw $t4 0($sp)
lw $t5 4($sp)
lw $t6 8($sp)
lw $t7 12($sp)
lw $t8 16($sp)
lw $t9 20($sp)
lw $s0 24($sp)
lw $s1 28($sp)
lw $s2 32($sp)
lw $s3 36($sp)
lw $s4 40($sp)
lw $s5 44($sp)
lw $s6 48($sp)
lw $s7 52($sp)
lw $t0 56($sp)
lw $t1 60($sp)
lw $t2 64($sp)
lw $t3 68($sp)
lw $ra -4($fp)
lw $fp 72($sp)
addu $sp $sp 80
j $ra
.text
.globl  List_GetEnd
List_GetEnd:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 16
sw $ra -4($fp)
sw $t8 0($sp)
sw $t9 4($sp)
move $t9 $a0
lw $t8 12($t9)
move $v0 $t8
lw $t8 0($sp)
lw $t9 4($sp)
lw $ra -4($fp)
lw $fp 8($sp)
addu $sp $sp 16
j $ra
.text
.globl  List_GetElem
List_GetElem:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 16
sw $ra -4($fp)
sw $t8 0($sp)
sw $t9 4($sp)
move $t9 $a0
lw $t8 4($t9)
move $v0 $t8
lw $t8 0($sp)
lw $t9 4($sp)
lw $ra -4($fp)
lw $fp 8($sp)
addu $sp $sp 16
j $ra
.text
.globl  List_GetNext
List_GetNext:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 16
sw $ra -4($fp)
sw $t8 0($sp)
sw $t9 4($sp)
move $t9 $a0
lw $t8 8($t9)
move $v0 $t8
lw $t8 0($sp)
lw $t9 4($sp)
lw $ra -4($fp)
lw $fp 8($sp)
addu $sp $sp 16
j $ra
.text
.globl  List_Print
List_Print:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 80
sw $ra -4($fp)
sw $t4 0($sp)
sw $t5 4($sp)
sw $t6 8($sp)
sw $t7 12($sp)
sw $t8 16($sp)
sw $t9 20($sp)
sw $s0 24($sp)
sw $s1 28($sp)
sw $s2 32($sp)
sw $s3 36($sp)
sw $s4 40($sp)
sw $s5 44($sp)
sw $s6 48($sp)
sw $s7 52($sp)
sw $t0 56($sp)
sw $t1 60($sp)
sw $t2 64($sp)
sw $t3 68($sp)
move $t9 $a0
move $t8 $t9
lw $t7 12($t9)
move $t6 $t7
lw $t5 4($t9)
move $t4 $t5
L31: nop
li $t3 1
sub $t2 $t3 $t6
beqz $t2 L32
move $t1 $t4
lw $t0 0($t1)
lw $s7 4($t0)
move $a0 $t1
jalr $s7
move $s6 $v0
move $a0 $s6
jal _print
move $s5 $t8
lw $s4 0($s5)
lw $s3 32($s4)
move $a0 $s5
jalr $s3
move $s2 $v0
move $t8 $s2
move $s1 $t8
lw $s0 0($s1)
lw $t7 24($s0)
move $a0 $s1
jalr $t7
move $t9 $v0
move $t6 $t9
move $t5 $t8
lw $t3 0($t5)
lw $t2 28($t3)
move $a0 $t5
jalr $t2
move $t0 $v0
move $t4 $t0
b L31
L32: nop
li $t1 1
move $v0 $t1
lw $t4 0($sp)
lw $t5 4($sp)
lw $t6 8($sp)
lw $t7 12($sp)
lw $t8 16($sp)
lw $t9 20($sp)
lw $s0 24($sp)
lw $s1 28($sp)
lw $s2 32($sp)
lw $s3 36($sp)
lw $s4 40($sp)
lw $s5 44($sp)
lw $s6 48($sp)
lw $s7 52($sp)
lw $t0 56($sp)
lw $t1 60($sp)
lw $t2 64($sp)
lw $t3 68($sp)
lw $ra -4($fp)
lw $fp 72($sp)
addu $sp $sp 80
j $ra
.text
.globl  LL_Start
LL_Start:
sw $fp -8($sp)
move $fp $sp
subu $sp $sp 80
sw $ra -4($fp)
sw $s0 0($sp)
sw $s1 4($sp)
sw $s2 8($sp)
sw $s3 12($sp)
sw $s4 16($sp)
sw $s5 20($sp)
sw $s6 24($sp)
sw $s7 28($sp)
sw $t0 32($sp)
sw $t1 36($sp)
sw $t2 40($sp)
sw $t3 44($sp)
sw $t4 48($sp)
sw $t5 52($sp)
sw $t6 56($sp)
sw $t7 60($sp)
sw $t8 64($sp)
sw $t9 68($sp)
li $t9 40
move $a0 $t9
jal _halloc
move $t8 $v0
move $t7 $t8
li $t6 16
move $a0 $t6
jal _halloc
move $t5 $v0
move $t4 $t5
la $t3 List_Print
sw $t3 36($t7)
la $t2 List_GetNext
sw $t2 32($t7)
la $t1 List_GetElem
sw $t1 28($t7)
la $t0 List_GetEnd
sw $t0 24($t7)
la $s7 List_Search
sw $s7 20($t7)
la $s6 List_Delete
sw $s6 16($t7)
la $s5 List_SetNext
sw $s5 12($t7)
la $s4 List_Insert
sw $s4 8($t7)
la $s3 List_InitNew
sw $s3 4($t7)
la $s2 List_Init
sw $s2 0($t7)
li $s1 4
move $s0 $s1
L33: nop
li $t9 15
sle $t8 $s0 $t9
beqz $t8 L34
add $t6 $t4 $s0
li $t5 0
sw $t5 0($t6)
li $t3 4
add $t2 $s0 $t3
move $s0 $t2
b L33
L34: nop
sw $t7 0($t4)
move $t1 $t4
move $t0 $t1
lw $s7 0($t0)
lw $s6 0($s7)
move $a0 $t0
jalr $s6
move $s5 $v0
move $s4 $t1
move $s3 $s4
lw $s2 0($s3)
lw $s1 0($s2)
move $a0 $s3
jalr $s1
move $t9 $v0
move $t8 $s4
lw $t6 0($t8)
lw $t5 36($t6)
move $a0 $t8
jalr $t5
move $t3 $v0
li $s0 24
move $a0 $s0
jal _halloc
move $t4 $v0
move $t2 $t4
li $t7 16
move $a0 $t7
jal _halloc
move $s7 $v0
move $t0 $s7
la $s6 Element_Compare
sw $s6 20($t2)
la $t1 Element_Equal
sw $t1 16($t2)
la $s5 Element_GetMarried
sw $s5 12($t2)
la $s2 Element_GetSalary
sw $s2 8($t2)
la $s3 Element_GetAge
sw $s3 4($t2)
la $s1 Element_Init
sw $s1 0($t2)
li $t9 4
move $t6 $t9
L35: nop
li $t8 15
sle $t5 $t6 $t8
beqz $t5 L36
add $t3 $t0 $t6
li $s0 0
sw $s0 0($t3)
li $t4 4
add $t7 $t6 $t4
move $t6 $t7
b L35
L36: nop
sw $t2 0($t0)
move $s7 $t0
move $s6 $s7
lw $t1 0($s6)
lw $s5 0($t1)
li $s2 25
li $s3 37000
li $s1 0
move $a0 $s6
move $a1 $s2
move $a2 $s3
move $a3 $s1
jalr $s5
move $t9 $v0
move $t8 $s4
lw $t5 0($t8)
lw $t3 8($t5)
move $a0 $t8
move $a1 $s7
jalr $t3
move $s0 $v0
move $s4 $s0
move $t4 $s4
lw $t6 0($t4)
lw $t0 36($t6)
move $a0 $t4
jalr $t0
move $t7 $v0
li $t2 10000000
move $a0 $t2
jal _print
li $t1 24
move $a0 $t1
jal _halloc
move $s6 $v0
move $s2 $s6
li $s1 16
move $a0 $s1
jal _halloc
move $s5 $v0
move $t9 $s5
la $s3 Element_Compare
sw $s3 20($s2)
la $t5 Element_Equal
sw $t5 16($s2)
la $t8 Element_GetMarried
sw $t8 12($s2)
la $t3 Element_GetSalary
sw $t3 8($s2)
la $s0 Element_GetAge
sw $s0 4($s2)
la $t6 Element_Init
sw $t6 0($s2)
li $t4 4
move $t0 $t4
L37: nop
li $t7 15
sle $t2 $t0 $t7
beqz $t2 L38
add $t1 $t9 $t0
li $s6 0
sw $s6 0($t1)
li $s1 4
add $s5 $t0 $s1
move $t0 $s5
b L37
L38: nop
sw $s2 0($t9)
move $s7 $t9
move $s3 $s7
lw $t5 0($s3)
lw $t8 0($t5)
li $t3 39
li $s0 42000
li $t6 1
move $a0 $s3
move $a1 $t3
move $a2 $s0
move $a3 $t6
jalr $t8
move $t4 $v0
move $t7 $s7
move $t2 $s4
lw $t1 0($t2)
lw $s6 8($t1)
move $a0 $t2
move $a1 $s7
jalr $s6
move $s1 $v0
move $s4 $s1
move $t0 $s4
lw $t9 0($t0)
lw $s5 36($t9)
move $a0 $t0
jalr $s5
move $s2 $v0
li $t5 10000000
move $a0 $t5
jal _print
li $s3 24
move $a0 $s3
jal _halloc
move $t3 $v0
move $t6 $t3
li $t8 16
move $a0 $t8
jal _halloc
move $t4 $v0
move $s0 $t4
la $t1 Element_Compare
sw $t1 20($t6)
la $t2 Element_Equal
sw $t2 16($t6)
la $s6 Element_GetMarried
sw $s6 12($t6)
la $s1 Element_GetSalary
sw $s1 8($t6)
la $t9 Element_GetAge
sw $t9 4($t6)
la $t0 Element_Init
sw $t0 0($t6)
li $s5 4
move $s2 $s5
L39: nop
li $t5 15
sle $s3 $s2 $t5
beqz $s3 L40
add $t3 $s0 $s2
li $t8 0
sw $t8 0($t3)
li $t4 4
add $t1 $s2 $t4
move $s2 $t1
b L39
L40: nop
sw $t6 0($s0)
move $s7 $s0
move $t2 $s7
lw $s6 0($t2)
lw $s1 0($s6)
li $t9 22
li $t0 34000
li $s5 0
move $a0 $t2
move $a1 $t9
move $a2 $t0
move $a3 $s5
jalr $s1
move $t5 $v0
move $s3 $s4
lw $t3 0($s3)
lw $t8 8($t3)
move $a0 $s3
move $a1 $s7
jalr $t8
move $t4 $v0
move $s4 $t4
move $s2 $s4
lw $s0 0($s2)
lw $t1 36($s0)
move $a0 $s2
jalr $t1
move $t6 $v0
li $s6 24
move $a0 $s6
jal _halloc
move $t2 $v0
move $t9 $t2
li $s5 16
move $a0 $s5
jal _halloc
move $s1 $v0
move $t5 $s1
la $t0 Element_Compare
sw $t0 20($t9)
la $t3 Element_Equal
sw $t3 16($t9)
la $s3 Element_GetMarried
sw $s3 12($t9)
la $t8 Element_GetSalary
sw $t8 8($t9)
la $t4 Element_GetAge
sw $t4 4($t9)
la $s0 Element_Init
sw $s0 0($t9)
li $s2 4
move $t1 $s2
L41: nop
li $t6 15
sle $s6 $t1 $t6
beqz $s6 L42
add $t2 $t5 $t1
li $s5 0
sw $s5 0($t2)
li $s1 4
add $t0 $t1 $s1
move $t1 $t0
b L41
L42: nop
sw $t9 0($t5)
move $t3 $t5
move $s3 $t3
lw $t8 0($s3)
lw $t4 0($t8)
li $s0 27
li $s2 34000
li $t6 0
move $a0 $s3
move $a1 $s0
move $a2 $s2
move $a3 $t6
jalr $t4
move $s6 $v0
move $t2 $s4
lw $s5 0($t2)
lw $s1 20($s5)
move $a0 $t2
move $a1 $t7
jalr $s1
move $t1 $v0
move $a0 $t1
jal _print
move $t5 $s4
lw $t0 0($t5)
lw $t9 20($t0)
move $a0 $t5
move $a1 $t3
jalr $t9
move $t8 $v0
move $a0 $t8
jal _print
li $s3 10000000
move $a0 $s3
jal _print
li $s0 24
move $a0 $s0
jal _halloc
move $t6 $v0
move $t4 $t6
li $s6 16
move $a0 $s6
jal _halloc
move $s2 $v0
move $s5 $s2
la $t2 Element_Compare
sw $t2 20($t4)
la $s1 Element_Equal
sw $s1 16($t4)
la $t1 Element_GetMarried
sw $t1 12($t4)
la $t0 Element_GetSalary
sw $t0 8($t4)
la $t3 Element_GetAge
sw $t3 4($t4)
la $t9 Element_Init
sw $t9 0($t4)
li $t5 4
move $t8 $t5
L43: nop
li $s3 15
sle $s0 $t8 $s3
beqz $s0 L44
add $t6 $s5 $t8
li $s6 0
sw $s6 0($t6)
li $s2 4
add $t2 $t8 $s2
move $t8 $t2
b L43
L44: nop
sw $t4 0($s5)
move $s7 $s5
move $s1 $s7
lw $t1 0($s1)
lw $t0 0($t1)
li $t3 28
li $t9 35000
li $t5 0
move $a0 $s1
move $a1 $t3
move $a2 $t9
move $a3 $t5
jalr $t0
move $s3 $v0
move $s0 $s4
lw $t6 0($s0)
lw $s6 8($t6)
move $a0 $s0
move $a1 $s7
jalr $s6
move $s2 $v0
move $s4 $s2
move $t8 $s4
lw $s5 0($t8)
lw $t2 36($s5)
move $a0 $t8
jalr $t2
move $t4 $v0
li $t1 2220000
move $a0 $t1
jal _print
move $s1 $s4
lw $t3 0($s1)
lw $t5 16($t3)
move $a0 $s1
move $a1 $t7
jalr $t5
move $t0 $v0
move $s4 $t0
move $s3 $s4
lw $t9 0($s3)
lw $t6 36($t9)
move $a0 $s3
jalr $t6
move $s0 $v0
li $s6 33300000
move $a0 $s6
jal _print
move $s2 $s4
lw $s5 0($s2)
lw $t8 16($s5)
move $a0 $s2
move $a1 $s7
jalr $t8
move $t2 $v0
move $s4 $t2
move $t4 $s4
lw $t1 0($t4)
lw $t3 36($t1)
move $a0 $t4
jalr $t3
move $t7 $v0
li $t5 44440000
move $a0 $t5
jal _print
li $s1 0
move $v0 $s1
lw $s0 0($sp)
lw $s1 4($sp)
lw $s2 8($sp)
lw $s3 12($sp)
lw $s4 16($sp)
lw $s5 20($sp)
lw $s6 24($sp)
lw $s7 28($sp)
lw $t0 32($sp)
lw $t1 36($sp)
lw $t2 40($sp)
lw $t3 44($sp)
lw $t4 48($sp)
lw $t5 52($sp)
lw $t6 56($sp)
lw $t7 60($sp)
lw $t8 64($sp)
lw $t9 68($sp)
lw $ra -4($fp)
lw $fp 72($sp)
addu $sp $sp 80
j $ra
.text
         .globl _halloc
_halloc:
         li $v0, 9
         syscall
         j $ra

         .text
         .globl _print
_print:
         li $v0, 1
         syscall
         la $a0, newl
         li $v0, 4
         syscall
         j $ra

         .data
         .align   0
newl:    .asciiz "\n" 
         .data
         .align   0
str_er:  .asciiz " ERROR: abnormal termination\n" 
